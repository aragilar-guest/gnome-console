# Portuguese translation for console.
# Copyright (C) 2022 console's COPYRIGHT HOLDER
# This file is distributed under the same license as the console package.
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: console main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/console/issues\n"
"POT-Creation-Date: 2022-02-14 22:03+0000\n"
"PO-Revision-Date: 2022-02-15 13:18+0000\n"
"Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>\n"
"Language-Team: Portuguese <pt@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0.1\n"

#: data/org.gnome.Console.desktop.in.in:3
#: data/org.gnome.Console.metainfo.xml.in.in:8 src/kgx-application.h:46
msgid "Console"
msgstr "Consola"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Console.desktop.in.in:8
msgid "command;prompt;cmd;commandline;run;shell;terminal;kgx;kings cross;"
msgstr ""
"comando;command;prompt;cmd;consola;linha de comandos;commandline;executar;"
"run;shell;terminal;kgx;kings cross;"

#: data/org.gnome.Console.desktop.in.in:21
msgid "New Window"
msgstr "Nova janela"

#: data/org.gnome.Console.desktop.in.in:27 src/kgx-window.ui:168
msgid "New Tab"
msgstr "Novo separador"

#: data/org.gnome.Console.metainfo.xml.in.in:9 src/kgx-application.c:568
msgid "Terminal Emulator"
msgstr "Emulador de terminal"

#: data/org.gnome.Console.metainfo.xml.in.in:11
msgid "A simple user-friendly terminal emulator for the GNOME desktop."
msgstr ""
"Um emulador de terminal simples e fácil de usar para o ambiente de trabalho "
"GNOME."

#: data/org.gnome.Console.metainfo.xml.in.in:30
msgid "Terminal window"
msgstr "Janela do terminal"

#: data/org.gnome.Console.metainfo.xml.in.in:75
msgid "Zander Brown"
msgstr "Zander Brown"

#: src/help-overlay.ui:13
msgctxt "shortcut window"
msgid "Application"
msgstr "Aplicação"

#: src/help-overlay.ui:19
msgctxt "shortcut window"
msgid "New Window"
msgstr "Nova janela"

#: src/help-overlay.ui:26
msgctxt "shortcut window"
msgid "Terminal"
msgstr "Terminal"

#: src/help-overlay.ui:32
msgctxt "shortcut window"
msgid "Find"
msgstr "Localizar"

#: src/help-overlay.ui:39
msgctxt "shortcut window"
msgid "Copy"
msgstr "Copiar"

#: src/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Paste"
msgstr "Colar"

#: src/help-overlay.ui:53
msgctxt "shortcut window"
msgid "Tabs"
msgstr "Separadores"

#: src/help-overlay.ui:59
msgctxt "shortcut window"
msgid "New Tab"
msgstr "Novo separador"

#: src/help-overlay.ui:66
msgctxt "shortcut window"
msgid "Close Tab"
msgstr "Fechar separador"

#: src/help-overlay.ui:73
msgctxt "shortcut window"
msgid "Next Tab"
msgstr "Separador seguinte"

#: src/help-overlay.ui:80
msgctxt "shortcut window"
msgid "Previous Tab"
msgstr "Separador anterior"

#: src/help-overlay.ui:87
msgctxt "shortcut window"
msgid "Switch to Tab"
msgstr "Mudar para separador"

#. Translators: The leading # is intentional, the initial %s is the
#. version of KGX itself, the latter format is the VTE version
#: src/kgx-application.c:540
#, c-format
msgid "# KGX %s using VTE %u.%u.%u %s\n"
msgstr "# KGX %s a usar VTE %u.%u.%u %s\n"

#. Translators: %s is the year range
#: src/kgx-application.c:552 src/kgx-window.c:525
#, c-format
msgid "© %s Zander Brown"
msgstr "© %s Zander Brown"

#: src/kgx-application.c:566 src/kgx-window.ui:115
msgid "King’s Cross"
msgstr "King’s Cross"

#: src/kgx-application.c:570
msgid "GPL 3.0 or later"
msgstr "GPL 3.0 ou posterior"

#: src/kgx-application.c:701
msgid "Execute the argument to this option inside the terminal"
msgstr "Executa o argumento para esta opção dentro do terminal"

#: src/kgx-application.c:710
msgid "Set the working directory"
msgstr "Define o diretório de trabalho"

#. Translators: Placeholder of for a given directory
#: src/kgx-application.c:712
msgid "DIRNAME"
msgstr "NOME-DIR"

#: src/kgx-application.c:720
msgid "Wait until the child exits (TODO)"
msgstr "Aguarda até o sub-processo sair (ToDo)"

#: src/kgx-application.c:729
msgid "Set the initial window title"
msgstr "Define o título da janela inicial"

#: src/kgx-application.c:738
msgid "ADVANCED: Set the shell to launch"
msgstr "AVANÇADO: Define o shell a lançar"

#: src/kgx-application.c:747
msgid "ADVANCED: Set the scrollback length"
msgstr "AVANÇADO: Define o comprimento do buffer de deslocamento (scrollback)"

#: src/kgx-application.h:44
msgid "Console (Development)"
msgstr "Consola (Desenvolvimento)"

#: src/kgx-close-dialog.c:50
msgid "Close Window?"
msgstr "Fechar janela?"

#: src/kgx-close-dialog.c:51
msgid ""
"Some commands are still running, closing this window will kill them and may "
"lead to unexpected outcomes"
msgstr ""
"Alguns comandos ainda estão em execução, fechar esta janela vai terminá-los "
"e pode levar a resultados inesperados"

#: src/kgx-close-dialog.c:56
msgid "Close Tab?"
msgstr "Fechar o separador?"

#: src/kgx-close-dialog.c:57
msgid ""
"Some commands are still running, closing this tab will kill them and may "
"lead to unexpected outcomes"
msgstr ""
"Alguns comandos ainda estão em execução, fechar este separador vai terminá-"
"los e pode levar a resultados inesperados"

#: src/kgx-close-dialog.ui:24 src/kgx-terminal.c:780
msgid "_Cancel"
msgstr "_Cancelar"

#: src/kgx-close-dialog.ui:32
msgid "C_lose"
msgstr "_Fechar"

#: src/kgx-pages.ui:47
msgid "_Detach Tab"
msgstr "_Desanexar separador"

#: src/kgx-pages.ui:53
msgid "_Close"
msgstr "_Fechar"

#. translators: <b> </b> marks the text as bold, ensure they are
#. matched please!
#: src/kgx-simple-tab.c:168
#, c-format
msgid "<b>Read Only</b> — Command exited with code %i"
msgstr "<b>Apenas leitura</b> — Comando encerrado com código %i"

#. translators: <b> </b> marks the text as bold, ensure they are
#. matched please!
#: src/kgx-simple-tab.c:177
msgid "<b>Read Only</b> — Command exited"
msgstr "<b>Apenas leitura</b> — Comando encerrado"

#. translators: <b> </b> marks the text as bold, ensure they are
#. matched please!
#: src/kgx-simple-tab.c:210
#, c-format
msgid "<b>Failed to start</b> — %s"
msgstr "<b>Falha ao iniciar</b> — %s"

#: src/kgx-tab.c:1160
msgid "Command completed"
msgstr "Comando concluído"

#: src/kgx-tab-button.ui:5
msgid "View Open Tabs"
msgstr "Ver separadores abertos"

#: src/kgx-terminal.c:772
msgid "You are pasting a command that runs as an administrator"
msgstr "Está a colar um comando que executa como administrador"

#. TRANSLATORS: %s is the command being pasted
#: src/kgx-terminal.c:775
#, c-format
msgid ""
"Make sure you know what the command does:\n"
"%s"
msgstr ""
"Certifique-se de que sabe o que o comando faz:\n"
"%s"

#: src/kgx-terminal.c:783 src/menus.ui:24
msgid "_Paste"
msgstr "C_olar"

#: src/kgx-theme-switcher.ui:18
msgid "Use System Colors"
msgstr "Usar cores do sistema"

#: src/kgx-theme-switcher.ui:47
msgid "Use Light Colors"
msgstr "Usar cores claras"

#: src/kgx-theme-switcher.ui:76
msgid "Use Dark Colors"
msgstr "Usar cores escuras"

#. Translators: Credit yourself here
#: src/kgx-window.c:531
msgid "translator-credits"
msgstr "Hugo Carvalho <hugokarvalho@hotmail.com>"

#: src/kgx-window.ui:26
msgid "Shrink Text"
msgstr "Reduzir texto"

#: src/kgx-window.ui:39
msgid "Reset Size"
msgstr "Repor tamanho"

#: src/kgx-window.ui:52
msgid "Enlarge Text"
msgstr "Aumentar texto"

#: src/kgx-window.ui:74
msgid "_New Window"
msgstr "_Nova janela"

#: src/kgx-window.ui:86
msgid "_Keyboard Shortcuts"
msgstr "_Teclas de atalho"

#: src/kgx-window.ui:93
msgid "_About Console"
msgstr "_Acerca da Consola"

#: src/kgx-window.ui:94
msgid "About this Program"
msgstr "Acerca deste programa"

#: src/kgx-window.ui:123
msgid "Find in Terminal"
msgstr "Localizar no terminal"

#: src/kgx-window.ui:141
msgid "Menu"
msgstr "Menu"

#: src/menus.ui:7
msgid "_Open Link"
msgstr "Abrir ligaçã_o"

#: src/menus.ui:12
msgid "Copy _Link"
msgstr "Copiar _ligação"

#: src/menus.ui:19
msgid "_Copy"
msgstr "_Copiar"

#: src/menus.ui:29
msgid "_Select All"
msgstr "_Selecionar tudo"

#: src/menus.ui:36
msgid "Show in _Files"
msgstr "Mostrar no Ficheiros"

#: nautilus/kgx-nautilus-menu-item.c:120
msgid "Open in Co_nsole (Devel)"
msgstr "Abrir na Co_nsola (Desenv.)"

#: nautilus/kgx-nautilus-menu-item.c:122
msgid "Open in Co_nsole"
msgstr "Abrir na Co_nsola"

#: nautilus/kgx-nautilus-menu-item.c:124
msgid "Start a terminal session for this location"
msgstr "Inicia uma sessão de terminal para este local"
